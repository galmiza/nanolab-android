## Synopsis

[Nanolab-Android](https://play.google.com/store/apps/details?id=net.galmiza.android.nanolab) is a mobile application available on Google Play store.  

It implements the [nanolab language java implementation](https://bitbucket.org/galmiza/nanolab-java) and lets you interact with the powerful command line interface, plot graphs and much more.

## Implementation

Among other features, this Android implementation  

  - extends the nanolab language features to interact with Android system and the plotting library
  - uses a local SQL database to store nanolab sessions using **android.database.sqlite.SQLiteDatabase**
  - extends the **ActionBarActivity** class to customize the action bar icons

## License

[zlib](https://en.wikipedia.org/wiki/Zlib_License)