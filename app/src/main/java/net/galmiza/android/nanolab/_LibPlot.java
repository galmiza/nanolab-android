package net.galmiza.android.nanolab;

import net.galmiza.nanolab.engine.Error;
import net.galmiza.nanolab.engine.Function.Pointer;
import net.galmiza.nanolab.engine.Node;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;

public class _LibPlot extends _LibToolsAndroid {

	// Error messages
	static private final String ERROR_OPTIONS_FORMAT = "Options must be set as a list of \"key\":\"value\"";
	static private final String ERROR_INVALID_COLOR = "Invalid color";

	// Options
	static private XYMultipleSeriesRenderer multipleRendererDefault = new XYMultipleSeriesRenderer();
	
	// Init default options
	// Some default values are weird, overwrite them
	static void initMultipleRendererDefaultOptions() {
		XYMultipleSeriesRenderer r = multipleRendererDefault;
		r.setApplyBackgroundColor(true);
		r.setZoomRate(5);
		r.setZoomButtonsVisible(true);
		r.setAntialiasing(true);
		r.setBackgroundColor(Color.WHITE);
		r.setMarginsColor(Color.WHITE);
		r.setAxesColor(Color.DKGRAY);
		r.setGridColor(Color.GRAY);
		r.setLabelsColor(Color.DKGRAY);
		r.setAxisTitleTextSize(20);
		r.setChartTitleTextSize(30);
		r.setLabelsTextSize(15);
		r.setLegendTextSize(15);
		r.setPointSize(5);
	}
	
	// Multiple series renderer default options
	static void applyMultipleRendererDefaultOptions(XYMultipleSeriesRenderer renderer) {

		XYMultipleSeriesRenderer r = multipleRendererDefault;
		renderer.setChartTitle(r.getChartTitle());
		renderer.setXTitle(r.getXTitle());
		renderer.setYTitle(r.getYTitle());
		
		renderer.setZoomButtonsVisible(r.isZoomButtonsVisible());
		renderer.setApplyBackgroundColor(r.isApplyBackgroundColor());
		renderer.setAntialiasing(r.isAntialiasing());
		renderer.setDisplayValues(r.isDisplayValues());
		renderer.setFitLegend(r.isFitLegend());
		renderer.setShowAxes(r.isShowAxes());
		renderer.setShowCustomTextGrid(r.isShowCustomTextGrid());
		renderer.setShowGridX(r.isShowGridX());
		renderer.setShowGridY(r.isShowGridY());
		renderer.setShowLabels(r.isShowLabels());
		renderer.setShowLegend(r.isShowLegend());
		renderer.setXRoundedLabels(r.isXRoundedLabels());

		renderer.setBackgroundColor(r.getBackgroundColor());
		renderer.setMarginsColor(r.getMarginsColor());
		renderer.setAxesColor(r.getAxesColor());
		renderer.setGridColor(r.getGridColor());
		renderer.setLabelsColor(r.getLabelsColor());
		renderer.setXLabelsColor(r.getXLabelsColor());
		renderer.setLegendHeight(r.getLegendHeight());
		renderer.setXLabels(r.getXLabels());
		renderer.setYLabels(r.getYLabels());
		
		renderer.setAxisTitleTextSize(r.getAxisTitleTextSize());
		renderer.setBarSpacing(r.getBarSpacing());
		renderer.setBarWidth(r.getBarWidth());
		renderer.setPointSize(r.getPointSize());
		renderer.setChartTitleTextSize(r.getChartTitleTextSize());
		renderer.setLabelsTextSize(r.getLabelsTextSize());
		renderer.setLegendTextSize(r.getLegendTextSize());
		renderer.setScale(r.getScale());
		renderer.setStartAngle(r.getStartAngle());
		renderer.setXAxisMax(r.getXAxisMax());
		renderer.setXAxisMin(r.getXAxisMin());
		renderer.setXLabelsAngle(r.getXLabelsAngle());
		renderer.setXLabelsPadding(r.getXLabelsPadding());
		renderer.setYAxisMax(r.getYAxisMax());
		renderer.setYAxisMin(r.getYAxisMin());
		renderer.setYLabelsAngle(r.getYLabelsAngle());
		renderer.setYLabelsPadding(r.getYLabelsPadding());
		renderer.setYLabelsVerticalPadding(r.getYLabelsVerticalPadding());

		renderer.setInitialRange(r.getInitialRange());
		renderer.setMargins(r.getMargins());
		renderer.setRange(r.getInitialRange());
		renderer.setXLabelsAlign(r.getXLabelsAlign());
		//renderer.setYLabelsAlign(multipleRendererDefault.getYLabelsAlign(0));
	}
	
	// Multiple series renderer custom options
	static void setMultipleSeriesRendererOptions(Node opts) throws Exception {
		for (Node n : opts.getChildren()) {
			if (n.isList()) {
				if (n.getChildrenCount()==2) {
					Node k = n.getChild(0);
					Node v = n.getChild(1);
					if (k.isString() && v.isString()) {
						String key = k.getString();
						String value = v.getString();
						XYMultipleSeriesRenderer r = multipleRendererDefault;
		
						// String
						if (key.equals("chartTitle"))	r.setChartTitle(value);
						if (key.equals("xTitle"))		r.setXTitle(value);
						if (key.equals("yTitle"))		r.setYTitle(value);
						
						// Boolean
						boolean b = value.equals("true") || value.equals("1");
						if (key.equals("antialisasing"))		r.setAntialiasing(b);
						if (key.equals("applyBackgroundColor"))	r.setApplyBackgroundColor(b);
						if (key.equals("displayValues"))		r.setDisplayValues(b);
						if (key.equals("fitLegend"))			r.setFitLegend(b);
						if (key.equals("showAxes"))				r.setShowAxes(b);
						if (key.equals("showCustomTextGrid"))	r.setShowCustomTextGrid(b);
						if (key.equals("showGrid"))				r.setShowGrid(b);
						if (key.equals("showGridX"))			r.setShowGridX(b);
						if (key.equals("showGridY"))			r.setShowGridY(b);
						if (key.equals("showLabels"))			r.setShowLabels(b);
						if (key.equals("showLegend"))			r.setShowLegend(b);
						if (key.equals("xRoundedLabels"))		r.setXRoundedLabels(b);
						
						// Int
						if (key.equals("axesColor"))		r.setAxesColor(Integer.valueOf(value));
						if (key.equals("backgroundColor"))	r.setBackgroundColor(Integer.valueOf(value));
						if (key.equals("gridColor"))		r.setGridColor(Integer.valueOf(value));
						if (key.equals("labelsColor"))		r.setLabelsColor(Integer.valueOf(value));
						if (key.equals("marginsColor"))		r.setMarginsColor(Integer.valueOf(value));
						if (key.equals("xLabelsColor"))		r.setXLabelsColor(Integer.valueOf(value));
						if (key.equals("legendHeight"))		r.setLegendHeight(Integer.valueOf(value));
						if (key.equals("xLabels"))			r.setXLabels(Integer.valueOf(value));
						if (key.equals("yLabels"))			r.setYLabels(Integer.valueOf(value));
						
						// Float
						if (key.equals("axisTitleTextSize"))	r.setAxisTitleTextSize(Float.valueOf(value));
						if (key.equals("barSpacing"))	r.setBarSpacing(Float.valueOf(value));
						if (key.equals("barWidth"))		r.setBarWidth(Float.valueOf(value));
						if (key.equals("pointSize"))	r.setPointSize(Float.valueOf(value));
						if (key.equals("chartTitleTextSize"))	r.setChartTitleTextSize(Float.valueOf(value));
						if (key.equals("labelsTextSize"))	r.setLabelsTextSize(Float.valueOf(value));
						if (key.equals("legendTextSize"))	r.setLegendTextSize(Float.valueOf(value));
						if (key.equals("scale"))		r.setScale(Float.valueOf(value));
						if (key.equals("startAngle"))	r.setStartAngle(Float.valueOf(value));
						if (key.equals("xAxisMax"))		r.setXAxisMax(Float.valueOf(value));
						if (key.equals("xAxisMin"))		r.setXAxisMin(Float.valueOf(value));
						if (key.equals("xLabelsAngle"))	r.setXLabelsAngle(Float.valueOf(value));
						if (key.equals("xLabelsPadding"))	r.setXLabelsPadding(Float.valueOf(value));
						if (key.equals("yAxisMax"))		r.setYAxisMax(Float.valueOf(value));
						if (key.equals("yAxisMin"))		r.setYAxisMin(Float.valueOf(value));
						if (key.equals("yLabelsAngle"))	r.setYLabelsAngle(Float.valueOf(value));
						if (key.equals("yLabelsPadding"))	r.setYLabelsPadding(Float.valueOf(value));
						if (key.equals("yLabelsVerticalPadding"))	r.setYLabelsVerticalPadding(Float.valueOf(value));
						
						// Other TODO
						/*r.setInitialRange(new double[] {});
						r.setMargins(new int[] {});
						r.setRange(new double[] {});
						r.setXLabelsAlign(Align.CENTER);
						r.setYLabelsAlign(Align.CENTER);*/
						
					} else throw new Exception(ERROR_OPTIONS_FORMAT);
				} else throw new Exception(ERROR_OPTIONS_FORMAT);
			} else throw new Exception(ERROR_OPTIONS_FORMAT);
		}
	}
	
	
	
	// Series renderer custom options
	static private void setXYSeriesRendererOptions(Node opts, XYSeriesRenderer renderer) throws Exception {
		for (Node n : opts.getChildren()) {
			if (n.isList()) {
				if (n.getChildrenCount()==2) {
					Node k = n.getChild(0);
					Node v = n.getChild(1);
					if (k.isString() && v.isString()) {
						String key = k.getString();
						String value = v.getString();

						// Boolean options
						boolean b = value.equals("true") || value.equals("1");
						if (key.equals("showLegendItem"))	renderer.setShowLegendItem(b);
						if (key.equals("highlighted"))		renderer.setHighlighted(b);
						if (key.equals("fillPoints"))		renderer.setFillPoints(b);
						if (key.equals("displayChartValues"))	renderer.setDisplayChartValues(b);
						if (key.equals("displayBoundingPoints"))	renderer.setDisplayBoundingPoints(b);
							
						// Float options
						if (key.equals("chartValuesSpacing"))	renderer.setChartValuesSpacing(Float.valueOf(value));
						if (key.equals("pointStrokeWidth"))		renderer.setPointStrokeWidth(Float.valueOf(value));
						if (key.equals("lineWidth"))			renderer.setLineWidth(Float.valueOf(value));
						if (key.equals("chartValuesTextSize"))	renderer.setChartValuesTextSize(Float.valueOf(value));
							
						// Integer options
						if (key.equals("displayChartValuesDistance"))	renderer.setDisplayChartValuesDistance(Integer.valueOf(value));
							
						// STYLE
						if (key.equals("style")) {
							if (value.equals("circle"))		renderer.setPointStyle(PointStyle.CIRCLE);
							if (value.equals("diamond"))	renderer.setPointStyle(PointStyle.DIAMOND);
							if (value.equals("point"))		renderer.setPointStyle(PointStyle.POINT);
							if (value.equals("square"))		renderer.setPointStyle(PointStyle.SQUARE);
							if (value.equals("triangle"))	renderer.setPointStyle(PointStyle.TRIANGLE);
							if (value.equals("x"))			renderer.setPointStyle(PointStyle.X);
						}
							
						// ALIGN
						if (key.equals("align")) {
							if (value.equals("center"))		renderer.setChartValuesTextAlign(Align.CENTER);
							if (value.equals("left"))		renderer.setChartValuesTextAlign(Align.LEFT);
							if (value.equals("right"))		renderer.setChartValuesTextAlign(Align.RIGHT);
						}
						
						// STROKE
						if (key.equals("stroke")) {
							if (value.equals("solid"))	renderer.setStroke(BasicStroke.SOLID);
							if (value.equals("dotted"))	renderer.setStroke(BasicStroke.DOTTED);
							if (value.equals("dashed"))	renderer.setStroke(BasicStroke.DASHED);
						}
							
						// COLOR
						if (key.equals("color"))		renderer.setColor(getColorFromString(value));
						
							
					} else throw new Exception(ERROR_OPTIONS_FORMAT);
				} else throw new Exception(ERROR_OPTIONS_FORMAT);
			} else throw new Exception(ERROR_OPTIONS_FORMAT);
		}
	}
	
	static private String getOption(Node opts, String key) throws Exception {
		for (Node n : opts.getChildren()) {
			if (n.isList()) {
				if (n.getChildrenCount()==2) {
					Node k = n.getChild(0);
					Node v = n.getChild(1);
					if (k.isString() && v.isString()) {
						if (k.getString().equals(key))
							return v.getString();
					} else throw new Exception(ERROR_OPTIONS_FORMAT);
				} else throw new Exception(ERROR_OPTIONS_FORMAT);
			} else throw new Exception(ERROR_OPTIONS_FORMAT);
		}
		return null;
	}
	
	private static void loadChart(Node node, XYMultipleSeriesDataset dataset, XYMultipleSeriesRenderer renderer) throws Exception {
		int remainingChildren = node.getChildrenCount();
		int id = 0;
		
		// Process all plot requests
		while (remainingChildren>=2) {
			
			// Create series
			Node x = node.getChild(id+0);
			Node y = node.getChild(id+1);
			XYSeries series = getXYSeries("", x, y);
			id += 2;
			remainingChildren -= 2;
			
			// Apply local options
			XYSeriesRenderer r = new XYSeriesRenderer();
			if (remainingChildren>0) {
				if (node.getChild(id).isList()) {
					setXYSeriesRendererOptions(node.getChild(id), r);
					String title = getOption(node.getChild(id), "seriesTitle");
					if (title != null) series.setTitle(title);
				}
				remainingChildren--;
				id++;
			}
			
			// Create dataset and renderer
			dataset.addSeries(series);
			renderer.addSeriesRenderer(r);
		}
	}
	
	static Pointer plotOptions() {  // plotOptions(opts)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					Node opts = node.getChild(0);
					if (opts.isList())		setMultipleSeriesRendererOptions(opts);
					else					throw new Exception(ERROR_OPTIONS_FORMAT);
					return node.setVoid();
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
			}
		};
	}
	
	static Pointer plot() { // plot(x,y) ; plot(x,y,opts) ; plot(x,y,opts,x,y,opts,...)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
									
				// Init global renderer
				final XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
				final XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
				applyMultipleRendererDefaultOptions(renderer);
				loadChart(node, dataset, renderer);
		        
				// Show graph
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Intent i = ChartFactory.getLineChartIntent(activity.getApplicationContext(), dataset, renderer);
				        activity.startActivity(i);
					}
				});
				return node.setVoid();
			}
		};
	}
	
	
	static Pointer hist() { // hist(x,y) ; hist(x,y,opts) ; hist(x,y,opts,x,y,opts,...)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==0) throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_COUNT);
									
				// Init global renderer
				final XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
				final XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
				applyMultipleRendererDefaultOptions(renderer);
				
				// First param is bar type
				int barType = 0;
				Node type = node.getChild(0);
				if (type.isString()) {
					if (type.getString().equals("default")) barType = 0;
					if (type.getString().equals("stacked")) barType = 1;
				} else
					throw new Exception("First parameter should be \"default\" or \"stacked\"");
				node.deleteChild(0);
				
				// Process graph data
				loadChart(node, dataset, renderer);
					
		        
				// Show graph
				final int barType_ = barType;
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Intent i = ChartFactory.getBarChartIntent(activity.getApplicationContext(),
								dataset,
								renderer,
								(barType_==1)?Type.STACKED:Type.DEFAULT);
				        activity.startActivity(i);
					}
				});
				return node.setVoid();
			}
		};
	}	

	
	
	static Pointer scatter() { // scatter(x,y) ; scatter(x,y,opts) ; scatter(x,y,opts,x,y,opts,...)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
									
				// Init global renderer
				final XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
				final XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
				applyMultipleRendererDefaultOptions(renderer);
				loadChart(node, dataset, renderer);
		        
				// Show graph
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Intent i = ChartFactory.getScatterChartIntent(activity.getApplicationContext(), dataset, renderer);
				        activity.startActivity(i);
					}
				});
				return node.setVoid();
			}
		};
	}
	
	
	static Pointer pie() { // pie({category:value(:color)}) ; pie({category:value},opts)
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				
				// Dataset
				if (node.getChildrenCount()>0) {
					if (node.getChild(0).isList()) {
						final CategorySeries categories = new CategorySeries("");
						final DefaultRenderer renderer = new DefaultRenderer();
						renderer.setLegendTextSize(25); //TODO make parameter
						renderer.setLabelsTextSize(20); //TODO make parameter
						renderer.setLabelsColor(Color.DKGRAY); //TODO make parameter
						
						for (Node n : node.getChild(0).getChildren()) {
							if (n.isList()) {
								if (n.getChildrenCount()>=2) {
									
									// Get values
									Node s = n.getChild(0);
									Node v = n.getChild(1);
									double value = v.getDoubleValue();
									if (s.isString())		categories.add(s.getString(), value);
									else throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
									
									// Renderer
									SimpleSeriesRenderer r = new SimpleSeriesRenderer();
									r.setChartValuesTextSize(20); //TODO make parameter
									if (n.getChildrenCount()>=3) r.setColor(getColorFromString(n.getChild(2).getString()));
									renderer.addSeriesRenderer(r);
									
								} else throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
							} else throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
						}
						
						// Show graph
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Intent i = ChartFactory.getPieChartIntent(activity.getApplicationContext(), categories, renderer, null);
						        activity.startActivity(i);
							}
						});
						
					} else throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				return node.setVoid();
			}
		};
	}
	
	
	
	// Return a int representing the color given as a string
	static private int getColorFromString(String s) throws Exception {
		if (s.equals("black"))	return Color.BLACK;
		if (s.equals("blue"))	return Color.BLUE;
		if (s.equals("cyan"))	return Color.CYAN;
		if (s.equals("dkgray"))	return Color.DKGRAY;
		if (s.equals("gray"))	return Color.GRAY;
		if (s.equals("green"))	return Color.GREEN;
		if (s.equals("ltgray"))	return Color.LTGRAY;
		if (s.equals("magenta"))	return Color.MAGENTA;
		if (s.equals("red"))	return Color.RED;
		if (s.equals("transparent"))	return Color.TRANSPARENT;
		if (s.equals("white"))	return Color.WHITE;
		if (s.equals("yellow"))	return Color.YELLOW;
		if (s.startsWith("#"))	return Color.parseColor(s);
		throw new Exception(ERROR_INVALID_COLOR);
	}
	
	
	// Create a xy series from nodes
	static private XYSeries getXYSeries(String title, Node x, Node y) throws Exception {
		XYSeries series = new XYSeries(title);
		for (int i=0; i<x.getChildrenCount(); i++) 
			series.add(x.getChild(i).getDoubleValue(), y.getChild(i).getDoubleValue());
		return series;
	}
	
}
