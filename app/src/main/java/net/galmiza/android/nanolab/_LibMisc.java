package net.galmiza.android.nanolab;

import net.galmiza.nanolab.engine.Error;
import net.galmiza.nanolab.engine.Function.Pointer;
import net.galmiza.nanolab.engine.Node;
import android.app.AlertDialog;
import android.widget.Toast;

public class _LibMisc extends _LibToolsAndroid {
	
	
	/**
	 * Android toast
	 */
	static Pointer toast() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					final Node n1 = node.getChild(0);
					final Node n2 = node.getChild(1);
					if (n1.isString() && n2.isInteger())
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(
										activity,
										n1.getString(),
										n2.getInteger().intValue()==0 ? Toast.LENGTH_SHORT:Toast.LENGTH_LONG).show();
							}
						});
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 2));
				return node.setVoid();
			}
		};
	}
	

	static Pointer message() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==2) {
					final Node title = node.getChild(0);
					final Node message = node.getChild(1);
					if (title.isString() && message.isString()) {
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								AlertDialog.Builder alert = new AlertDialog.Builder(activity);
								alert.setTitle(title.getString());
								alert.setMessage(message.getString());		
								alert.show();
							}
						});
					} else
						throw new Exception(Error.EXECUTION_WRONG_ARGUMENT_TYPE);
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
				return node.setVoid();
			}
		};
	}
	
	static Pointer print() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==1) {
					main.addTextViewResponse(node.getChild(0).toString());
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 1));
				return node.setVoid();
			}
		};
	}

	static Pointer clear() {
		return new Pointer() {
			@Override
			public Node run(Node node) throws Exception {
				if (node.getChildrenCount()==0) {
					activity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							linearLayout.removeAllViews();
						}
					});
					
				} else
					throw new Exception(parser.getArgumentCountExceptionString(this, 0));
				return node.setVoid();
			}
		};
	}	
}
