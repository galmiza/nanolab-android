package net.galmiza.android.nanolab;

import net.galmiza.nanolab.engine.Function;
import net.galmiza.nanolab.engine.Init;
import net.galmiza.nanolab.engine.Node;
import net.galmiza.nanolab.engine.Parser;
import net.galmiza.nanolab.engine._LibTools;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
 
public class MainActivity extends ActionBarActivity {

	// Constant
	private static final int INTENT_RESULT_SETTINGS = 0;
	private static final int INTENT_RESULT_VARIABLES = 1;
	private static final int INTENT_RESULT_FUNCTIONS = 2;
	//private static final int INTENT_RESULT_THREADS = 3;
	private static final int INTENT_RESULT_SESSIONS = 4;
	public static final String CURRENT_SESSION = "__current__"; // __current__
	
	// Attributes
	private Parser parser;
	private SessionManager sessionManager;
	private Session currentSession;
	private ActionBar actionBar;
	private Thread primaryThread;
	
	// GUI objects
	private EditText editTextInput;
	private ImageButton imageButtonInputPlay;
	private ImageButton imageButtonInputStop;
	private ImageButton imageButtonInputClear;
	private LinearLayout linearLayoutOutput;
	private ScrollView scrollViewOutput;
	
	// TODO to preferences
	private static final int CONSOLE_INPUT_BACKGROUND_COLOR = Color.parseColor("#DDDDDD");
	private static final int CONSOLE_OUTPUT_TEXT_COLOR = Color.parseColor("#00AA00");
	private static final int CONSOLE_OUTPUT_TEXT_ERROR_COLOR = Color.parseColor("#FF0000");
	
	/**
	 * Entry point
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Action bar
		actionBar = getSupportActionBar();
		actionBar.setTitle(getString(R.string.app_name));
		actionBar.setSubtitle(getString(R.string.app_description));
		
		// GUI
		editTextInput = (EditText) findViewById(R.id.edittext_input);
		imageButtonInputPlay = (ImageButton) findViewById(R.id.imagebutton_input_play);
		imageButtonInputStop = (ImageButton) findViewById(R.id.imagebutton_input_stop);
		imageButtonInputClear = (ImageButton) findViewById(R.id.imagebutton_input_clear);
		linearLayoutOutput = (LinearLayout) findViewById(R.id.linearlayout_output);
		scrollViewOutput = (ScrollView) findViewById(R.id.scrollview_output);

		// Give Pointers access to key variables
		_LibToolsAndroid.activity = this;
		_LibToolsAndroid.linearLayout = linearLayoutOutput;
		_LibToolsAndroid.main = this;
		SessionsActivity.activity = this;
		SessionsActivity.main = this;
		
		// Define buttons behaviour
		imageButtonInputPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {				
				String input = editTextInput.getText().toString();
				addTextViewRequest(input);
				runAsyncExecution(input);
				imageButtonInputPlay.setVisibility(View.GONE);
				imageButtonInputStop.setVisibility(View.VISIBLE);
			}
		});
		imageButtonInputStop.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	}
		});
		imageButtonInputClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	editTextInput.setText("");		}
		});
		
		
		//System.out.println(Obfuscator.getDexFileSize(this));
		
		// Preferences
		final ProgressDialog progress = util.Misc.progressDialog(this, "Loading session", "Please wait...", false, false);
		final boolean auto_save = util.Misc.getPreference(this,"session_autosave", true);
		if (auto_save)
			progress.show();
		
		// Parser and session 
		final Activity activity = this;
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					// Create parser
					parser = new Parser();
					initParser();
					
					// Load parser preferences (decimal precisions)
					parser.setDecimalPrintPrecision(Integer.valueOf(util.Misc.getPreference(activity,"decimal_print_precision", String.valueOf(parser.getDecimalPrintPrecision()))));
					parser.setDecimalExecutionPrecision(Integer.valueOf(util.Misc.getPreference(activity,"decimal_execution_precision", String.valueOf(parser.getDecimalExecutionPrecision()))));
					
					// Create managers
					sessionManager = new SessionManager(activity);
					currentSession = sessionManager.getSessionFromName(CURRENT_SESSION);
					if (currentSession==null) {
						sessionManager.createSession(new Session(CURRENT_SESSION, "Auto saved/restored session"));
						currentSession = sessionManager.getSessionFromName(CURRENT_SESSION);
					}
					SessionsActivity.sessionManager = sessionManager;
					
					// Load current session
					if (auto_save) {
						sessionManager.loadSessionIntoWorkspace(currentSession);
						if (currentSession.commands != null)
							for (int i=0; i<currentSession.commands.length; i++)
								addTextViewOutput(currentSession.commands[i], currentSession.types[i]);
					}

					// Init plot options
					_LibPlot.initMultipleRendererDefaultOptions();
					
					progress.dismiss();
					
				} catch (Exception e) {
					util.Misc.Toast(activity, "Unable to init system!", Toast.LENGTH_SHORT);
					e.printStackTrace();
					progress.dismiss();
				}
			}	
		}).start();
	}

	
	/**
	 * Initiate the parser
	 */
	private void initParser() throws Exception {

		// Share reference
		_LibTools.parser = parser;
		VariablesActivity.parser = parser;
		FunctionsActivity.parser = parser;
		SessionManager.parser = parser;
		
		// Load default
		Init.loadOperators(parser);
		Init.loadVariables(parser);
		Init.loadNativeFunctions(parser);
		Init.loadScriptedFunctions(parser);
		
		// Load Android functions
		parser.addFunction("toast", _LibMisc.toast(), Function.TYPE_BUILT_IN);
		parser.addFunction("print", _LibMisc.print(), Function.TYPE_BUILT_IN);
		parser.addFunction("message", _LibMisc.message(), Function.TYPE_BUILT_IN);
		parser.addFunction("clear", _LibMisc.clear(), Function.TYPE_BUILT_IN);
		parser.addFunction("hist", _LibPlot.hist(), Function.TYPE_BUILT_IN);
		parser.addFunction("pie", _LibPlot.pie(), Function.TYPE_BUILT_IN);
		parser.addFunction("scatter", _LibPlot.scatter(), Function.TYPE_BUILT_IN);
		parser.addFunction("plot", _LibPlot.plot(), Function.TYPE_BUILT_IN);
		parser.addFunction("plotOptions", _LibPlot.plotOptions(), Function.TYPE_BUILT_IN);
	}

	/**
	 * Stop execution
	 */
	private void stopPrimaryThread() {
		if (primaryThread!=null) {
			Node.stop();
			while (primaryThread.isAlive()) {
				try { Thread.sleep(10); }
				catch (Exception e) { e.printStackTrace(); }
			}
		}
			
	}
	
	
	/**
	 * Run primary thread
	 */
	void runAsyncExecution(final String s) {
		stopPrimaryThread();
		primaryThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					boolean output = false;
					String out = null;
					
					// Prevent sending empty string
					if (s.length()>0) {
						String input = s;
						
						// If last char is ;, user requests no output
						if (input.charAt(input.length()-1)==';')
							input = input.substring(0,input.length()-1);
						else 
							output = true;
						
						out = parser.runExpression(input);
					}
					
					// Update view
					final boolean output_ = output;
					final String out_ = out;
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (output_) {
								// Print output
								addTextViewResponse("> "+out_);
								scollOutputDown();
							}
							// Switch button visibility
							imageButtonInputStop.setVisibility(View.GONE);
							imageButtonInputPlay.setVisibility(View.VISIBLE);
						}
					});
					
				// Error management
				} catch (Exception e) {				addTextViewException("> "+e.getMessage());
				} catch (OutOfMemoryError e) {		addTextViewException("> Out of memory");
				} catch (StackOverflowError e) {	addTextViewException("> Recursive expression");
					
				} finally {
					// Show play button
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							imageButtonInputStop.setVisibility(View.GONE);
							imageButtonInputPlay.setVisibility(View.VISIBLE);
							scollOutputDown();
						}
					});
				}
				
			}
		});
		Node.start();
		primaryThread.start();
	}
	
	/**
	 * Force scrollview down (to reveal new content)
	 */
	public void scollOutputDown() {
		scrollViewOutput.post(new Runnable() {            
		    @Override
		    public void run() {
		    	scrollViewOutput.fullScroll(View.FOCUS_DOWN);              
		    }
		});
	}
	
	
	/**
	 * Response to preferences and billing service
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    switch (requestCode) {
	    
	    // Reload decimal precision preferences
	    case INTENT_RESULT_SETTINGS:
		    parser.setDecimalPrintPrecision(Integer.valueOf(util.Misc.getPreference(this,"decimal_print_precision", String.valueOf(parser.getDecimalPrintPrecision()))));
			parser.setDecimalExecutionPrecision(Integer.valueOf(util.Misc.getPreference(this,"decimal_execution_precision", String.valueOf(parser.getDecimalExecutionPrecision()))));
			break;
			
	    // Load command history from current session
	    case INTENT_RESULT_SESSIONS:
	    	final ProgressDialog progress = util.Misc.progressDialog(this, "Loading command history", "Please wait...", false, false);
			progress.show();
			new Thread(new Runnable() {
				@Override
				public void run() {
					String[] commands = (String[]) util.Misc.getAttribute("commands");
					Integer[] types = (Integer[]) util.Misc.getAttribute("types");
					if (commands != null)
						for (int i=0; i<commands.length; i++) {
							addTextViewOutput(commands[i], types[i]);
							sessionManager.addSessionCommand(currentSession, commands[i], types[i]);
						}
					util.Misc.setAttribute("commands", null);
					util.Misc.setAttribute("types", null);
					progress.dismiss();
				}
			}).start();
			break;
	    }

	}
	
	@Override
	public void onBackPressed() {
		
		// Autosave
		if (util.Misc.getPreference(this,"session_autosave", true)) {
			final ProgressDialog progress = util.Misc.progressDialog(this, "Saving session", "Please wait...", false, false);
			progress.show();
			
			final Activity activity = this;
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						sessionManager.updateSession(currentSession);
					} catch (Exception e) {
						util.Misc.Toast(activity, "Unable to save current session!", Toast.LENGTH_SHORT);
						e.printStackTrace();
					}
					progress.dismiss();
					finish();
				}
			}).start();
		} else {
			finish();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (sessionManager!=null) sessionManager.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.actionbar_menu_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.action_bar_main_action_output_reset:
			linearLayoutOutput.removeAllViews();
			currentSession.commands = null;
			sessionManager.clearCommands(currentSession);
			break;
			
		case R.id.action_bar_main_action_workspace_reset:
			try {
				parser = new Parser();
				initParser();
			} catch (Exception e) {
				util.Misc.Toast(this, "Unable to reload engine!", Toast.LENGTH_SHORT);
				e.printStackTrace();
			}
			break;
		
		// Open preference activity
		case R.id.action_bar_main_action_settings:
			Intent intent = new Intent(this, PreferencesActivity.class);
			startActivityForResult(intent, INTENT_RESULT_SETTINGS);
			break;
			
		// Open popup to select
		case R.id.action_bar_action_main_change_view:
			final Activity a = this;
			View v = findViewById(R.id.action_bar_action_main_change_view);
			PopupMenu popupMenu = new PopupMenu(this, v);
			popupMenu.getMenuInflater().inflate(R.menu.popup_menu_views, popupMenu.getMenu());		
			popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					Intent intent;
					
					switch (item.getItemId()) {
					case R.id.popup_menu_view_variables:
						intent = new Intent(a, VariablesActivity.class);
						startActivityForResult(intent, INTENT_RESULT_VARIABLES);
						break;
					case R.id.popup_menu_view_functions:
						intent = new Intent(a, FunctionsActivity.class);
						startActivityForResult(intent, INTENT_RESULT_FUNCTIONS);
						break;
					case R.id.popup_menu_view_sessions:
						intent = new Intent(a, SessionsActivity.class);
						startActivityForResult(intent, INTENT_RESULT_SESSIONS);
						break;
					/*case R.id.popup_menu_view_threads:
						intent = new Intent(a, ThreadsActivity.class);
						startActivityForResult(intent, INTENT_RESULT_THREADS);
						break;*/
					}
					return true;
				}
			});
			popupMenu.show();
			break;
			
		}
		return true;
	}
	

	// Add a textview to layout for request/response/exception
	public void addTextViewRequest(String s) {
		addTextViewOutput(s, Session.TYPE_REQUEST);
		sessionManager.addSessionCommand(currentSession, s, Session.TYPE_REQUEST);
	}
	public void addTextViewResponse(String s) {
		addTextViewOutput(s, Session.TYPE_RESPONSE);
		sessionManager.addSessionCommand(currentSession, s, Session.TYPE_RESPONSE);
	}
	public void addTextViewException(String s) {
		addTextViewOutput(s, Session.TYPE_EXCEPTION);
		sessionManager.addSessionCommand(currentSession, s, Session.TYPE_EXCEPTION);
	}
	
	// Add a textview to the linear layout representing output
	public void addTextViewOutput(final String s, final int type) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				TextView tv = (TextView) getLayoutInflater().inflate(R.layout.console_item_textview, null);
				tv.setText(s);
				tv.setTag(s);
				switch (type) {
				case Session.TYPE_REQUEST:		tv.setBackgroundColor(MainActivity.CONSOLE_INPUT_BACKGROUND_COLOR);	break;
				case Session.TYPE_RESPONSE:		tv.setTextColor(MainActivity.CONSOLE_OUTPUT_TEXT_COLOR);			break;
				case Session.TYPE_EXCEPTION:	tv.setTextColor(MainActivity.CONSOLE_OUTPUT_TEXT_ERROR_COLOR);		break;
				}
				linearLayoutOutput.addView(tv);
				scollOutputDown();
			}
		});
	}
	
	// Detect click on items
	public void onOutputItemClick(View view) {
		String s = (String) view.getTag();
		editTextInput.setText(s);
	}
	
}
