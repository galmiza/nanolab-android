package net.galmiza.android.nanolab;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import util.Misc.OnMultiInputHintDialogListener;
import util.Misc.OnSimpleInputDialogListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SessionsActivity extends ActionBarActivity {
	
	// Constants
	private static final int INTENT_SESSION_PICK = 0;
	
	// Attributes
	private SessionAdapter sessionAdapter;
	private List<Session> sessions;
	private ActionBar actionBar;
	
	// References
	static SessionManager sessionManager;
	static Activity activity;
	static MainActivity main;
	
	/**
	 * On create
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sessions);
		
		// Init list
		sessions = sessionManager.getSessions();
		
		// Listview
		ListView listViewSessions = (ListView) findViewById(R.id.listViewSessions);
		sessionAdapter = new SessionAdapter(getBaseContext(), R.layout.item_session, sessions);
		listViewSessions.setAdapter(sessionAdapter);
		sessionAdapter.notifyDataSetChanged();
		
		// Action bar
		actionBar = getSupportActionBar();
		actionBar.setTitle(getString(R.string.app_name));
		
		updateView();
	}
	private void updateView() {
		actionBar.setSubtitle(sessions.size()+" sessions");
		sessionAdapter.notifyDataSetChanged();
	}
	
	/**
	 * Adapter
	 */
	public class SessionAdapter extends ArrayAdapter<Session> {
		
		private int resourceId;
		public SessionAdapter(Context context, int resourceId, List<Session> objects) {
			super(context, resourceId, objects);
			this.resourceId = resourceId;
		}
		
		@Override
	    public View getView(int position, View convertView, ViewGroup parent) {
			
	        View view = convertView;           
	        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        view = li.inflate(resourceId, null);
	        Session s = getItem(position);
	        
	        // Customize view elements 
            TextView textViewName = (TextView) view.findViewById(R.id.textview_session_item_name);
            TextView textViewTime = (TextView) view.findViewById(R.id.textview_session_item_time);
            TextView textViewDescription = (TextView) view.findViewById(R.id.textview_session_item_description);
            ImageButton imageButtonShare = (ImageButton) view.findViewById(R.id.imagebutton_session_item_share);
            ImageButton imageButtonExport = (ImageButton) view.findViewById(R.id.imagebutton_session_item_export);
            ImageButton imageButtonLoad = (ImageButton) view.findViewById(R.id.imagebutton_session_item_load);
	        
            textViewName.setText(s.name);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
			textViewTime.setText(sdf.format(new Date(s.time)));
			textViewDescription.setText(s.description);
			
			imageButtonShare.setTag(s);
			imageButtonExport.setTag(s);
			imageButtonLoad.setTag(s);
            view.setTag(s);
			return view;
		}
		
	}
	
	
	/**
	 * Menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.actionbar_menu_sessions, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case R.id.action_bar_sessions_action_new:
			onCreateSessionDialog();
			break;					
		case R.id.action_bar_sessions_action_import:
			onImportSessionDialog();
			break;
			
		}
		return true;
	}


	/**
	 * Response to activities
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, final Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    switch (requestCode) {
        case INTENT_SESSION_PICK:
        	if (resultCode == RESULT_OK) {
        		final Activity activity = this;
        		final ProgressDialog progress = util.Misc.progressDialog(activity, "Importing session", "Please wait...", false, false);
        		progress.show();
        		new Thread(new Runnable() {
        			@Override
        			public void run() {
        				try {
				        	Uri uri = data.getData();
							sessionManager.importSession(new File(uri.getPath()));
					        sessions.clear();
							sessions.addAll(sessionManager.getSessions());
							activity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									updateView();
									progress.dismiss();
								}
							});
						} catch (Exception e) {
							util.Misc.Toast(activity, "Couldn't import the session!", Toast.LENGTH_SHORT);
							e.printStackTrace();
						}
        			}
        		}).start();
        	}
		}
	}
	
	
	
	
	/**
	 * On create new session
	 */
	public void onCreateSessionDialog() {
		util.Misc.onMultiInputHintDialog(this, "Create new session", null, new String[] { "Name", "Description" }, null, new OnMultiInputHintDialogListener() {
			@Override
			public void onInput(String[] inputs) throws Exception {
				try {
					sessionManager.createSession(new Session(inputs[0], inputs[1]));
					sessions.clear();
					sessions.addAll(sessionManager.getSessions());
					updateView();
				} catch (Exception e) {
					util.Misc.Toast(activity, "Couldn't create the session!", Toast.LENGTH_SHORT);
					e.printStackTrace();
				}
			}
		});
	}
	
	
	/**
	 * On import session
	 */
	private void onImportSessionDialog() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT); 
	    intent.setType("*/*"); 
	    intent.addCategory(Intent.CATEGORY_OPENABLE);
	    try {
	        startActivityForResult(
	                Intent.createChooser(intent, "Select a session "),
	                INTENT_SESSION_PICK);
	    } catch (android.content.ActivityNotFoundException ex) {
	        Toast.makeText(this, "Please install a File Manager", Toast.LENGTH_SHORT).show();
	    }
	}
	
	
	
	
	
	
	public void onSessionMenuPopop(final Session s, View view) {
		int res = R.menu.session_popup_menu;
		
		// Create popup
		PopupMenu popupMenu = new PopupMenu(this, view);
		popupMenu.getMenuInflater().inflate(res, popupMenu.getMenu());		
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
				case R.id.session_item_action_load: 		onLoadSession(s);				break;
				case R.id.session_item_action_export: 		onExportSessionDialog(s);		break;
				case R.id.session_item_action_share: 		onShareSession(s);				break;
				case R.id.session_item_action_edit: 		onEditSessionDialog(s);			break;
				case R.id.session_item_action_delete: 		onDeleteSessionDialog(s);		break;
				}
				return true;
			}
		});
		popupMenu.show();
	}
	
	
	private void onEditSessionDialog(final Session s) {
		util.Misc.onMultiInputHintDialog(this, "Edit session", null,
				new String[] { "Name", "Description" },
				new String[] { s.name, s.description }, new OnMultiInputHintDialogListener() {
			@Override
			public void onInput(String[] inputs) throws Exception {
				try {
					sessionManager.updateSession(s, inputs[0], inputs[1]);
					sessions.clear();
					sessions.addAll(sessionManager.getSessions());
					updateView();
				} catch (Exception e) {
					util.Misc.Toast(activity, "Couldn't udpate the session!", Toast.LENGTH_SHORT);
					e.printStackTrace();
				}
			}
		});
	}
	private void onDeleteSessionDialog(final Session s) {
		util.Misc.onSimpleInputDialog(this, "Delete session", "Are you sure?", null, new OnSimpleInputDialogListener() {
			@Override
			public void onInput(String input) throws Exception {
				sessionManager.deleteSession(s);
				sessions.clear();
				sessions.addAll(sessionManager.getSessions());
				updateView();
			}
		});		
	}
	private void onExportSessionDialog(final Session s) {
		File path = new File(Environment.getExternalStorageDirectory(), util.Misc.getPreference(activity, "export_path", "nanolab"));
		util.Misc.onSimpleInputDialog(this, "Export session", "Session will be exported to "+path.getAbsolutePath(), s.name+".nls", new OnSimpleInputDialogListener() {
			@Override
			public void onInput(String input) throws Exception {
				try {
					sessionManager.export(s, input);
				} catch (Exception e) {
					util.Misc.Toast(activity, "Unable to export the session!", Toast.LENGTH_SHORT);
					e.printStackTrace();
				}
			}
		});
	}
	
	
	// Share the session (gmail, dropbox, whatever is installed on device...)
	private void onShareSession(final Session s) {
		try {
			File tmp = new File(Environment.getExternalStorageDirectory(), s.name+".nls"); // on external storage so that other application can access it!
			sessionManager.exportSessionToFile(s, tmp);
			Intent i = new Intent(Intent.ACTION_SEND);  
			i.setType("text/plain");
			i.putExtra(Intent.EXTRA_SUBJECT, "Nanolab session: "+s.name);
			i.putExtra(Intent.EXTRA_TEXT, s.description);
			i.putExtra(Intent.EXTRA_STREAM,	Uri.parse("file://"+tmp.getAbsolutePath()));
			activity.startActivity(Intent.createChooser(i, "Share the session"));
			tmp.deleteOnExit(); // on exit otherwise delete before it was sent!
		} catch (Exception e) {
			util.Misc.Toast(activity, "Unable to export the session!", Toast.LENGTH_SHORT);
			e.printStackTrace();
		}
	}
	private void onLoadSession(final Session s) {
		final ProgressDialog progress = util.Misc.progressDialog(this, "Loading session", "Please wait...", false, false);
		progress.show();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					sessionManager.loadSessionIntoWorkspace(s);
					util.Misc.setAttribute("commands", s.commands);
					util.Misc.setAttribute("types", s.types);
					util.Misc.Toast(activity, "Session loaded into workspace!", Toast.LENGTH_SHORT);
				} catch (Exception e) {
					util.Misc.Toast(activity, "Unable to load session!", Toast.LENGTH_SHORT);
					e.printStackTrace();
				}
				progress.dismiss();
			}
		}).start();

	}
	
	
	
	/**
	 * Actions on item click
	 */
	public void onItemClick(View view) {
		Session s = (Session) view.getTag();
		onSessionMenuPopop(s, view);
	}
	public void onExportSessionClick(View view) {
		onExportSessionDialog((Session) view.getTag());
	}
	public void onShareSessionClick(View view) {
		onShareSession((Session) view.getTag());
	}
	public void onLoadSessionClick(View view) {
		onLoadSession((Session) view.getTag());
	}
}
