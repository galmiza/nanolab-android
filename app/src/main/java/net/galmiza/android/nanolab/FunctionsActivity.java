package net.galmiza.android.nanolab;

import java.util.List;

import net.galmiza.nanolab.engine.Function;
import net.galmiza.nanolab.engine.Parser;
import util.Misc.OnSimpleInputDialogListener;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class FunctionsActivity extends ActionBarActivity {

	// References
	static Parser parser;
	
	// Attributes
	private FunctionAdapter functionAdapter;
	private List<Function> functions;
	private ActionBar actionBar;
	
	/**
	 * On create
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_functions);
		
		// Listview
		ListView listViewVariables = (ListView) findViewById(R.id.listViewFunctions);
		functions = parser.getScriptedFunctions();
		functionAdapter = new FunctionAdapter(getBaseContext(), R.layout.item_function, functions);
		listViewVariables.setAdapter(functionAdapter);

		// Action bar
		actionBar = getSupportActionBar();
		actionBar.setTitle(getString(R.string.app_name));
		
		updateView();
	}
	private void updateView() {
		actionBar.setSubtitle(functions.size()+" scripted functions");
		functionAdapter.notifyDataSetChanged();
	}
	
	
	
	/**
	 * Adapter
	 */
	public class FunctionAdapter extends ArrayAdapter<Function> {
		
		private int resourceId;
		public FunctionAdapter(Context context, int resourceId, List<Function> objects) {
			super(context, resourceId, objects);
			this.resourceId = resourceId;
		}
		
		@Override
	    public View getView(int position, View convertView, ViewGroup parent) {
			
	        View view = convertView;           
	        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        view = li.inflate(resourceId, null);
	        Function f = getItem(position);
	        
	        // Customize view elements 
            TextView textViewName = (TextView) view.findViewById(R.id.textview_function_item_name);
            TextView textViewExpression = (TextView) view.findViewById(R.id.textview_function_item_expression);

            textViewName.setText(f.target.toString());
            textViewExpression.setText(parser.removeExtraParenthesis(f.expression.toString()));
            
            view.setTag(f);
			return view;
		}
		
	}
	
	

	/**
	 * Menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.actionbar_menu_functions, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
					
		case R.id.action_bar_functions_action_new:
			onCreateFunctionDialog();
			break;
			
		}
		return true;
	}
	
	
	/**
	 * On click
	 */
	public void onItemClick(View view) {
		Function f = (Function) view.getTag();
		onFunctionMenuPopop(f, view);
	}
	
	public void onFunctionMenuPopop(final Function f, View view) {
		int res = R.menu.function_popup_menu;
		
		// Create popup
		PopupMenu popupMenu = new PopupMenu(this, view);
		popupMenu.getMenuInflater().inflate(res, popupMenu.getMenu());		
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
				case R.id.function_item_action_edit: 		onEditFunctionDialog(f);		break;
				case R.id.function_item_action_delete: 		onDeleteFunctionDialog(f);		break;
				}
				return true;
			}
		});
		popupMenu.show();
	}

	/**
	 * On create new function
	 */
	public void onCreateFunctionDialog() {
		util.Misc.onSimpleInputDialog(this, "New function", "Enter the declaration expression", "name(argv)=expression", new OnSimpleInputDialogListener() {
			@Override
			public void onInput(String input) throws Exception {
				parser.runExpression(input);
				functions.clear();
				functions.addAll(parser.getScriptedFunctions());
				updateView();
			}
		});
	}
	
	/**
	 * On rename variable
	 */
	public void onEditFunctionDialog(final Function f) {
		String init = String.format("%s=%s", f.target.toString(), parser.removeExtraParenthesis(f.expression.toString()));
		util.Misc.onSimpleInputDialog(this, "Edit function", "Please enter the new declaration.", init, new OnSimpleInputDialogListener() {
			@Override
			public void onInput(String input) throws Exception {
				parser.removeFunction(f.name);
				parser.runExpression(input);
				functions.clear();
				functions.addAll(parser.getScriptedFunctions());
				functionAdapter.notifyDataSetChanged();
			}
		});
	}	
	
	/**
	 * On delete variable
	 */
	public void onDeleteFunctionDialog(final Function f) {
		util.Misc.onSimpleInputDialog(this, "Delete function", "Are you sure?", null, new OnSimpleInputDialogListener() {
			@Override
			public void onInput(String input) throws Exception {
				parser.removeFunction(f.name);
				functions.clear();
				functions.addAll(parser.getScriptedFunctions());
				updateView();
			}
		});
	}
	
	
}
