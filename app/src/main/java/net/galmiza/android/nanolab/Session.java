package net.galmiza.android.nanolab;

import java.io.Serializable;
import java.util.Date;

public class Session implements Serializable {
	
	// Version
	private static final long serialVersionUID = 1L;

	// Constant
	static final int TYPE_REQUEST = 0;
	static final int TYPE_RESPONSE = 1;
	static final int TYPE_EXCEPTION = 2;
	
	// Attributes
	transient Integer id;
	String name;
	String description;
	Long time;
	
	// Data used for serialization only
	byte[] workspace;
	String[] commands;
	Integer[] types;
	
	
	// Constructors
	Session(String name, String description) {
		this.name = name;
		this.description = description;
		this.time = new Date().getTime();
	}
	Session(Integer id, String name, String description, Long time) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.time = time;
	}
}
