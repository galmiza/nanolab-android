package net.galmiza.android.nanolab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.galmiza.nanolab.engine.Parser;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

/**
 * Manage all sessions
 * 
 * A session is identified with ??
 * A session consists in:
 * -a command history
 * -variables
 * -scripted functions
 * 
 * 
 */
public class SessionManager extends SQLiteOpenHelper {
	
	// References
	static public Parser parser;
	private Activity activity;
	  
	// Constructor
	SessionManager(Activity activity) {
		super(activity.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
		this.activity = activity;
	}

	// Close db
	public void dispose() {
		getWritableDatabase().close();
	}

	// Add a session
	public void createSession(Session s) throws Exception {
		
		// Save workspace
		ContentValues values = new ContentValues();
	    values.put("name", s.name);
	    values.put("description", s.description);
	    values.put("creationDate", new Date().getTime());
	    values.put("workspace", getCurrentWorkspace());
	    s.id = (int) getWritableDatabase().insert("sessions", null, values);
	    
	    // Copy command history from the current session
	    long id = getSessionFromName(MainActivity.CURRENT_SESSION).id;
	    Cursor cursor = getWritableDatabase().query("commands", new String[] { "command", "type" }, "sessionId=="+id, null, null, null, null);
		if (cursor.moveToFirst()) {
			do  {
				values = new ContentValues();
				values.put("sessionId", s.id);
				values.put("command", cursor.getString(0));
				values.put("type", cursor.getInt(1));
				getWritableDatabase().insert("commands", null, values);
			} while (cursor.moveToNext());
		}
		cursor.close();
		
	}
	
	// Delete a session
	public void deleteSession(Session s) {
		getWritableDatabase().delete("sessions", "_id=="+s.id, null);
	}
	
	// Update name and description of a session (date is automatically updated)
	public void updateSession(Session s, String name, String description) {
		ContentValues values = new ContentValues();
		values.put("name", name);
		values.put("description", description);
	    values.put("creationDate", new Date().getTime());
		getWritableDatabase().update("sessions", values, "_id=="+s.id, null);
	}
	
	// Update workspace (date is automatically updated)
	public void updateSession(Session s) throws Exception {
		ContentValues values = new ContentValues();
	    values.put("creationDate", new Date().getTime());
	    values.put("workspace", getCurrentWorkspace());
		getWritableDatabase().update("sessions", values, "_id=="+s.id, null);
	}
	
	// Load a session to workspace
	public void loadSessionIntoWorkspace(Session s) throws Exception {
		loadSessionFromDB(s);
		
		// Load workspace
		File tmp = new File(activity.getFilesDir(), "tmp");
		util.File.writeBinary(tmp, s.workspace, false);
		parser.loadState(tmp);
		tmp.delete();
		s.workspace = null; // release memory
	}
	
	// Export session to file in default workspace
	public void exportSessionToFile(final Session s, File f) throws Exception {
		
		// Add workspace to session from db (not initially loaded for memory print reasons)
		loadSessionFromDB(s);
				
		// Serialize in a file
		ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(f));
		oos.writeObject(s);
		oos.close();
		
		// Release memory
		s.workspace = null;
		s.commands = null;
		s.types = null;
	}
	public void export(final Session s, String name) throws Exception {
		final File path = new File(Environment.getExternalStorageDirectory(), util.Misc.getPreference(activity, "export_path", "nanolab"));
		if (!path.exists()) path.mkdirs();
		exportSessionToFile(s, new File(path, name));
	}
	
	// Import session from a file to be selected by user
	public void importSession(final File f) throws Exception {
		
		ObjectInputStream ois;
		
		// Unserialize
		ois = new ObjectInputStream(new FileInputStream(f));
		Session s = (Session) ois.readObject();
		ois.close();
					
		// Save workspace into db
		ContentValues values = new ContentValues();
		values.put("name", s.name);
		values.put("description", s.description);
		values.put("creationDate", new Date().getTime());
		values.put("workspace", s.workspace);
		long id = getWritableDatabase().insert("sessions", null, values);
					
		// Save commands into db
		for (int i=0; i<s.commands.length; i++) {
			values = new ContentValues();
			values.put("sessionId", id);
			values.put("command", s.commands[i]);
			values.put("type", s.types[i]);
			getWritableDatabase().insert("commands", null, values);
		}
	}
	

	// Add a command line for the session
	public void addSessionCommand(Session s, String command, int type) {
		ContentValues values = new ContentValues();
		values.put("sessionId", s.id);
		values.put("command", command);
		values.put("type", type);
		getWritableDatabase().insert("commands", null, values);
	}
	public void clearCommands(Session s) {
		getWritableDatabase().delete("commands", "sessionId=="+s.id, null);
	}
	
	// Get list of sessions
	public List<Session> getSessions() {
		List<Session> sessions = new ArrayList<Session>();
		Cursor cursor = getWritableDatabase().query("sessions", new String[] { "_id", "name", "description", "creationDate" }, null, null, null, null, "creationDate DESC");
		if (cursor.moveToFirst()) {
			do  {
				Integer id = cursor.getInt(0);
				String name = cursor.getString(1);
				String description = cursor.getString(2);
				Long time = cursor.getLong(3);
				sessions.add(new Session(id, name, description, time));
			} while (cursor.moveToNext());
		}
		cursor.close();
		return sessions;
	}
	
	// Get session from name (only first match returned)
	public Session getSessionFromName(String name) {
		String c = "'"; // To avoid pre-obfuscation issues
		Cursor cursor = getWritableDatabase().query("sessions", new String[] { "_id", "name", "description", "creationDate" }, String.format("name==%s%s%s",c,name,c), null, null, null, "creationDate DESC");
		if (cursor.moveToFirst()) {
			Integer id = cursor.getInt(0);
			String description = cursor.getString(2);
			Long time = cursor.getLong(3);
			cursor.close();
			return new Session(id, name, description, time);
		} else {
			cursor.close();
			return null;
		}
	}
	
	// Return the serialize current workspace
	private byte[] getCurrentWorkspace() throws Exception {
		File tmp = new File(activity.getFilesDir(), "tmp");
		if (tmp.exists()) tmp.delete();
		parser.saveState(tmp);
	    byte[] b = util.File.readBinary(tmp);
	    tmp.delete();
	    return b;
	}
	// Extract workspace and commands from db to session in memory
	private void loadSessionFromDB(Session s) throws Exception {
		
		// Workspace
		Cursor cursor = getWritableDatabase().query("sessions", new String[] { "workspace" }, "_id=="+s.id, null, null, null, null);
		cursor.moveToFirst();
		s.workspace = cursor.getBlob(0);
		cursor.close();
		
		// Commands
		cursor = getWritableDatabase().query("commands", new String[] { "command", "type" }, "sessionId=="+s.id, null, null, null, null);
		if (cursor.moveToFirst()) {
			List<String> commands = new ArrayList<String>();
			List<Integer> types = new ArrayList<Integer>();
			do  {
				commands.add(cursor.getString(0));
				types.add(cursor.getInt(1));
			} while (cursor.moveToNext());
			s.commands = commands.toArray(new String[commands.size()]);
			s.types = types.toArray(new Integer[types.size()]);
		}
		cursor.close();
	}
	
	
	// Data management in SQL
	private static final String DATABASE_NAME = "sessions.db";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_CREATE_SESSIONS =
			"create table sessions ("
			+ "_id integer primary key autoincrement, "
			+ "name text not null,"
			+ "description text not null,"
			+ "creationDate long,"
			+ "workspace blob);";

	private static final String DATABASE_CREATE_COMMANDS =
			"create table commands ("
			+ "sessionId integer,"
			+ "command text not null,"
			+ "type integer);";
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE_SESSIONS);
		db.execSQL(DATABASE_CREATE_COMMANDS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS sessions");
		db.execSQL("DROP TABLE IF EXISTS commands");
	    onCreate(db);
	}
	
}
