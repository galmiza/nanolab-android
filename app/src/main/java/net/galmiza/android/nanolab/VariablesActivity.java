package net.galmiza.android.nanolab;

import java.util.List;

import net.galmiza.nanolab.engine.Parser;
import net.galmiza.nanolab.engine.Variable;
import util.Misc.OnSimpleInputDialogListener;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class VariablesActivity extends ActionBarActivity {

	// References
	static Parser parser;
	
	// Attributes
	private VariableAdapter variableAdapter;
	private ActionBar actionBar;
	
	/**
	 * On create
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_variables);
		
		// Listview
		ListView listViewVariables = (ListView) findViewById(R.id.listViewVariables);
		variableAdapter = new VariableAdapter(getBaseContext(), R.layout.item_variable, parser.getVariables());
		listViewVariables.setAdapter(variableAdapter);
		
		// Action bar
		actionBar = getSupportActionBar();
		actionBar.setTitle(getString(R.string.app_name));
		
		updateView();
	}
	private void updateView() {
		actionBar.setSubtitle(parser.getVariables().size()+" variables");
		variableAdapter.notifyDataSetChanged();
	}
	
	
	
	/**
	 * Adapter
	 */
	public class VariableAdapter extends ArrayAdapter<Variable> {
		
		private int resourceId;
		public VariableAdapter(Context context, int resourceId, List<Variable> objects) {
			super(context, resourceId, objects);
			this.resourceId = resourceId;
		}
		
		@Override
	    public View getView(int position, View convertView, ViewGroup parent) {
			
	        View view = convertView;           
	        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        view = li.inflate(resourceId, null);
	        Variable v = getItem(position);
	        
	        // Customize view elements 
            TextView textViewName = (TextView) view.findViewById(R.id.textview_variable_item_name);
            TextView textViewType = (TextView) view.findViewById(R.id.textview_variable_item_type);
            TextView textViewValue = (TextView) view.findViewById(R.id.textview_variable_item_value);
	        
            textViewName.setText(v.name);
            textViewType.setText(v.value.getTypeAsString());
            textViewValue.setText(v.value.toString());
            
            view.setTag(v);
			return view;
		}
		
	}
	
	

	/**
	 * Menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.actionbar_menu_variables, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
					
		case R.id.action_bar_variables_action_new:
			onCreateVariableDialog();
			break;
			
		}
		return true;
	}
	
	
	/**
	 * On click
	 */
	public void onItemClick(View view) {
		Variable v = (Variable) view.getTag();
		onSessionMenuPopop(v, view);
	}
	
	public void onSessionMenuPopop(final Variable v, View view) {
		int res = R.menu.variable_popup_menu;
		
		// Create popup
		PopupMenu popupMenu = new PopupMenu(this, view);
		popupMenu.getMenuInflater().inflate(res, popupMenu.getMenu());		
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
				case R.id.variable_item_action_rename: 		onRenameVariableDialog(v);		break;
				case R.id.variable_item_action_delete: 		onDeleteVariableDialog(v);		break;
				}
				return true;
			}
		});
		popupMenu.show();
	}

	/**
	 * On create new variable
	 */
	public void onCreateVariableDialog() {
		util.Misc.onSimpleInputDialog(this, "New variable", "Enter the affectation expression", "name=value", new OnSimpleInputDialogListener() {
			@Override
			public void onInput(String input) throws Exception {
				parser.runExpression(input);
				updateView();
			}
		});
	}
	
	/**
	 * On rename variable
	 */
	public void onRenameVariableDialog(final Variable v) {
		util.Misc.onSimpleInputDialog(this, "Rename variable", "Please set the new variable name.", v.name, new OnSimpleInputDialogListener() {
			@Override
			public void onInput(String input) throws Exception {
				parser.addVariable(input, v.value.copy(), 0);
				parser.removeVariable(v.name);
				updateView();
			}
		});
	}	
	
	/**
	 * On delete variable
	 */
	public void onDeleteVariableDialog(final Variable v) {
		util.Misc.onSimpleInputDialog(this, "Delete variable", "Are you sure?", null, new OnSimpleInputDialogListener() {
			@Override
			public void onInput(String input) throws Exception {
				parser.removeVariable(v.name);
				updateView();
			}
		});
	}
}
