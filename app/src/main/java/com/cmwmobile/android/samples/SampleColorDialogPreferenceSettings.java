/**
 * Copyright CMW Mobile.com, 2011. 
 */
package com.cmwmobile.android.samples;

import net.galmiza.android.nanolab.R;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * The SampleColorDialogPreferenceSettings is responsible for the handling of
 * this sample settings.
 * @author Casper Wakkers
 */
public class SampleColorDialogPreferenceSettings extends
		PreferenceActivity implements
		SharedPreferences.OnSharedPreferenceChangeListener {
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getPreferenceManager().setSharedPreferencesName(
			"com.cmwmobile.android.samples.color");
		addPreferencesFromResource(R.xml.preferences);
		getPreferenceManager().getSharedPreferences().
			registerOnSharedPreferenceChangeListener(this);
	}
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	protected void onDestroy() {
		getPreferenceManager().getSharedPreferences().
			unregisterOnSharedPreferenceChangeListener(this);

		super.onDestroy();
	}
	/**
	 * {@inheritDoc}
	 */
	protected void onResume() {
		super.onResume();
	}
	/**
	 * {@inheritDoc}
	 */
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
	}
}
